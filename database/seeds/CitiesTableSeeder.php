<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'id' => 1,
                'province_id' => 1,
                'name' => 'Banda Aceh' 
            ],
            [
                'id' => 2,
                'province_id' => 1,
                'name' => 'Langsa' 
            ],
            [
                'id' => 3,
                'province_id' => 1,
                'name' => 'Lhokseumawe' 
            ],
            [
                'id' => 4,
                'province_id' => 1,
                'name' => 'Meulaboh' 
            ],
            [
                'id' => 5,
                'province_id' => 1,
                'name' => 'Sabang' 
            ],
            [
                'id' => 6,
                'province_id' => 1,
                'name' => 'Subulussalam' 
            ],
            [
                'id' => 7,
                'province_id' => 2,
                'name' => 'Denpasar' 
            ],
            [
                'id' => 8,
                'province_id' => 3,
                'name' => 'Pangkalpinang' 
            ],
            [
                'id' => 9,
                'province_id' => 4,
                'name' => 'Cilegon'
            ],
            [
                'id' => 10,
                'province_id' => 4,
                'name' => 'Serang' 
            ],
            [
                'id' => 11,
                'province_id' => 4,
                'name' => 'Tangerang Selatan' 
            ],
            [
                'id' => 12,
                'province_id' => 4,
                'name' => 'Tangerang' 
            ],
            [
                'id' => 13,
                'province_id' => 5,
                'name' => 'Bengkulu' 
            ],
            [
                'id' => 14,
                'province_id' => 6,
                'name' => 'Gorontalo' 
            ],
            [
                'id' => 15,
                'province_id' => 7,
                'name' => 'Kota Administrasi Jakarta Barat' 
            ],
            [
                'id' => 16,
                'province_id' => 7,
                'name' => 'Kota Administrasi Jakarta Pusat' 
            ],
            [
                'id' => 17,
                'province_id' => 7,
                'name' => 'Kota Administrasi Jakarta Selatan' 
            ],
            [
                'id' => 18,
                'province_id' => 7,
                'name' => 'Kota Administrasi Jakarta Timur' 
            ],
            [
                'id' => 19,
                'province_id' => 7,
                'name' => 'Kota Administrasi Jakarta Utara' 
            ],
            [
                'id' => 20,
                'province_id' => 8,
                'name' => 'Sungai Penuh' 
            ],
            [
                'id' => 21,
                'province_id' => 8,
                'name' => 'Jambi' 
            ],
            [
                'id' => 22,
                'province_id' => 9,
                'name' => 'Bandung' 
            ],
            [
                'id' => 23,
                'province_id' => 9,
                'name' => 'Bekasi' 
            ],
            [
                'id' => 24,
                'province_id' => 9,
                'name' => 'Bogor' 
            ],
            [
                'id' => 25,
                'province_id' => 9,
                'name' => 'Cimahi   ' 
            ],
            [
                'id' => 26,
                'province_id' => 9,
                'name' => 'Cirebon' 
            ],
            [
                'id' => 27,
                'province_id' => 9,
                'name' => 'Depok' 
            ],
            [
                'id' => 28,
                'province_id' => 9,
                'name' => 'Sukabumi' 
            ],
            [
                'id' => 29,
                'province_id' => 9,
                'name' => 'Tasikmalaya' 
            ],
            [
                'id' => 30,
                'province_id' => 9,
                'name' => 'Banjar' 
            ],
            [
                'id' => 31,
                'province_id' => 10,
                'name' => 'Magelang' 
            ],
            [
                'id' => 32,
                'province_id' => 10,
                'name' => 'Pekalongan' 
            ],
            [
                'id' => 33,
                'province_id' => 10,
                'name' => 'Purwokerto' 
            ],
            [
                'id' => 34,
                'province_id' => 10,
                'name' => 'Salatiga' 
            ],
            [
                'id' => 35,
                'province_id' => 10,
                'name' => 'Semarang' 
            ],
            [
                'id' => 36,
                'province_id' => 10,
                'name' => 'Surakarta' 
            ],
            [
                'id' => 37,
                'province_id' => 10,
                'name' => 'Tegal' 
            ],
            [
                'id' => 38,
                'province_id' => 11,
                'name' => 'Batu' 
            ],
            [
                'id' => 39,
                'province_id' => 11,
                'name' => 'Blitar' 
            ],
            [
                'id' => 40,
                'province_id' => 11,
                'name' => 'Kediri' 
            ],
            [
                'id' => 41,
                'province_id' => 11,
                'name' => 'Madiun' 
            ],
            [
                'id' => 42,
                'province_id' => 11,
                'name' => 'Malang' 
            ],
            [
                'id' => 43,
                'province_id' => 11,
                'name' => 'Mojokerto' 
            ],
            [
                'id' => 44,
                'province_id' => 11,
                'name' => 'Pasuruan' 
            ],
            [
                'id' => 45,
                'province_id' => 11,
                'name' => 'Probolinggo' 
            ],
            [
                'id' => 46,
                'province_id' => 11,
                'name' => 'Surabaya' 
            ],
            [
                'id' => 47,
                'province_id' => 12,
                'name' => 'Pontianak' 
            ],
            [
                'id' => 48,
                'province_id' => 12,
                'name' => 'Singkawang' 
            ],
            [
                'id' => 49,
                'province_id' => 13,
                'name' => 'Banjarbaru' 
            ],
            [
                'id' => 50,
                'province_id' => 13,
                'name' => 'Banjarmasin' 
            ],
            [
                'id' => 51,
                'province_id' => 14,
                'name' => 'Palangkaraya' 
            ],
            [
                'id' => 52,
                'province_id' => 15,
                'name' => 'Balikpapan' 
            ],
            [
                'id' => 53,
                'province_id' => 15,
                'name' => 'Bontang' 
            ],
            [
                'id' => 54,
                'province_id' => 15,
                'name' => 'Samarinda' 
            ],
            [
                'id' => 55,
                'province_id' => 16,
                'name' => 'Tarakan' 
            ],
            [
                'id' => 56,
                'province_id' => 17,
                'name' => 'Batam' 
            ],
            [
                'id' => 57,
                'province_id' => 17,
                'name' => 'Tanjungpinang' 
            ],
            [
                'id' => 58,
                'province_id' => 18,
                'name' => 'Bandar Lampung' 
            ],
            [
                'id' => 59,
                'province_id' => 18,
                'name' => 'Metro' 
            ],
            [
                'id' => 60,
                'province_id' => 19,
                'name' => 'Ternate' 
            ],
            [
                'id' => 61,
                'province_id' => 19,
                'name' => 'Tidore Kepulauan' 
            ],
            [
                'id' => 62,
                'province_id' => 20,
                'name' => 'Ambon' 
            ],
            [
                'id' => 63,
                'province_id' => 20,
                'name' => 'Tual' 
            ],
            [
                'id' => 64,
                'province_id' => 21,
                'name' => 'Bima' 
            ],
            [
                'id' => 65,
                'province_id' => 21,
                'name' => 'Mataram' 
            ],
            [
                'id' => 66,
                'province_id' => 22,
                'name' => 'Kupang' 
            ],
            [
                'id' => 67,
                'province_id' => 23,
                'name' => 'Sorong' 
            ],
            [
                'id' => 68,
                'province_id' => 24,
                'name' => 'Jayapura' 
            ],
            [
                'id' => 69,
                'province_id' => 25,
                'name' => 'Dumai' 
            ],
            [
                'id' => 70,
                'province_id' => 25,
                'name' => 'Pekanbaru' 
            ],
            [
                'id' => 71,
                'province_id' => 26,
                'name' => 'Majene' 
            ],
            [
                'id' => 72,
                'province_id' => 26,
                'name' => 'Mamasa' 
            ],
            [
                'id' => 73,
                'province_id' => 26,
                'name' => 'Mamuju' 
            ],
            [
                'id' => 74,
                'province_id' => 26,
                'name' => 'Tobadak' 
            ],
            [
                'id' => 75,
                'province_id' => 26,
                'name' => 'Pasangkayu' 
            ],
            [
                'id' => 76,
                'province_id' => 26,
                'name' => 'Polewali' 
            ],
            [
                'id' => 77,
                'province_id' => 27,
                'name' => 'Makassar' 
            ],
            [
                'id' => 78,
                'province_id' => 27,
                'name' => 'Palopo' 
            ],
            [
                'id' => 79,
                'province_id' => 27,
                'name' => 'Parepare' 
            ],
            [
                'id' => 80,
                'province_id' => 28,
                'name' => 'Palu' 
            ],
            [
                'id' => 81,
                'province_id' => 29,
                'name' => 'Bau-Bau' 
            ],
            [
                'id' => 82,
                'province_id' => 29,
                'name' => 'Kendari' 
            ],
            [
                'id' => 83,
                'province_id' => 30,
                'name' => 'Bitung' 
            ],
            [
                'id' => 84,
                'province_id' => 30,
                'name' => 'Kotamobagu' 
            ],
            [
                'id' => 85,
                'province_id' => 30,
                'name' => 'Manado' 
            ],
            [
                'id' => 86,
                'province_id' => 30,
                'name' => 'Tomohon' 
            ],
            [
                'id' => 87,
                'province_id' => 31,
                'name' => 'Bukittinggi' 
            ],
            [
                'id' => 88,
                'province_id' => 31,
                'name' => 'Padang' 
            ],
            [
                'id' => 89,
                'province_id' => 31,
                'name' => 'Padangpanjang' 
            ],
            [
                'id' => 90,
                'province_id' => 31,
                'name' => 'Pariaman' 
            ],
            [
                'id' => 91,
                'province_id' => 31,
                'name' => 'Payakumbuh' 
            ],
            [
                'id' => 92,
                'province_id' => 31,
                'name' => 'Sawahlunto' 
            ],
            [
                'id' => 93,
                'province_id' => 31,
                'name' => 'Solok' 
            ],
            [
                'id' => 94,
                'province_id' => 32,
                'name' => 'Lubuklinggau' 
            ],
            [
                'id' => 95,
                'province_id' => 32,
                'name' => 'Pagaralam' 
            ],
            [
                'id' => 96,
                'province_id' => 32,
                'name' => 'Palembang' 
            ],
            [
                'id' => 97,
                'province_id' => 32,
                'name' => 'Prabumulih' 
            ],
            [
                'id' => 98,
                'province_id' => 33,
                'name' => 'Binjai' 
            ],
            [
                'id' => 99,
                'province_id' => 33,
                'name' => 'Medan' 
            ],
            [
                'id' => 100,
                'province_id' => 33,
                'name' => 'Padang Sidempuan' 
            ],
            [
                'id' => 101,
                'province_id' => 33,
                'name' => 'Pematangsiantar' 
            ],
            [
                'id' => 102,
                'province_id' => 33,
                'name' => 'Sibolga ' 
            ],
            [
                'id' => 103,
                'province_id' => 33,
                'name' => 'Tanjungbalai' 
            ],
            [
                'id' => 104,
                'province_id' => 33,
                'name' => 'Tebingtinggi' 
            ],
            [
                'id' => 105,
                'province_id' => 34,
                'name' => 'Yogyakarta' 
            ],
        ]);
    }
}
