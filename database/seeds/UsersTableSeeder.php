<?php
require 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            [
                'id' => 1,
                'uuid' => Uuid::uuid4(),
                'name' => 'Ridwan',
                'email_verified_at' => now(),
                'email' => 'ridwan@gmail.com',
                'password' => bcrypt('12345678'),
            ],
            [
                'id' => 2,
                'uuid' => Uuid::uuid4(),
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],
            [
                'id' => 3,
                'uuid' => Uuid::uuid4(),
                'name' => 'Member',
                'email' => 'member@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],
        ));
    }
}
