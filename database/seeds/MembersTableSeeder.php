<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->insert(array(
            [
                'user_id' => 1,
                'address' => 'Glagah Lor, Tamanan Bantul, DIY',
                'province' => '34',
                'city' => '105',
                'hp' => '0857123456789',
                'birth_date' => '1990-08-08',
                'gender' => 'Pria',
                'position' => 'Master'
            ],
            [
                'user_id' => 2,
                'address' => 'Glagah Lor, Tamanan Bantul, DIY',
                'province' => '34',
                'city' => '105',
                'hp' => '0857123456789',
                'birth_date' => '1990-08-08',
                'gender' => 'Pria',
                'position' => 'Admin'
            ],
            [
                'user_id' => 3,
                'address' => 'Glagah Lor, Tamanan Bantul, DIY',
                'province' => '34',
                'city' => '105',
                'hp' => '0857123456789',
                'birth_date' => '1990-08-08',
                'gender' => 'Pria',
                'position' => 'Member'
            ]
        ));
    }
}
