<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert([

            [
                'id' => 1,
                'name' => 'Aceh'
            ],
            [
                'id' => 2,
                'name' => 'Bali'
            ],
            [
                'id' => 3,
                'name' => 'Bangka Belitung'
            ],
            [
                'id' => 4,
                'name' => 'Banten'
            ],
            [
                'id' => 5,
                'name' => 'Bengkulu'
            ],
            [
                'id' => 6,
                'name' => 'Gorontalo'
            ],
            [
                'id' => 7,
                'name' => 'Jakarta'
            ],
            [
                'id' => 8,
                'name' => 'Jambi'
            ],
            [
                'id' => 9,
                'name' => 'Jawa Barat'
            ],
            [
                'id' => 10,
                'name' => 'Jawa Tengah'
            ],
            [
                'id' => 11,
                'name' => 'Jawa Timur'
            ],
            [
                'id' => 12,
                'name' => 'Kalimantan Barat'
            ],
            [
                'id' => 13,
                'name' => 'Kalimantan Selatan'
            ],
            [
                'id' => 14,
                'name' => 'Kalimantan Tengah'
            ],
            [
                'id' => 15,
                'name' => 'Kalimantan Timur'
            ],
            [
                'id' => 16,
                'name' => 'Kalimantan Utara'
            ],
            [
                'id' => 17,
                'name' => 'Kepulauan Riau'
            ],
            [
                'id' => 18,
                'name' => 'Lampung'
            ],
            [
                'id' => 19,
                'name' => 'Maluku Utara'
            ],
            [
                'id' => 20,
                'name' => 'Maluku'
            ],
            [
                'id' => 21,
                'name' => 'Nusa Tenggara Barat'
            ],
            [
                'id' => 22,
                'name' => 'Nusa Tenggara Timur'
            ],
            [
                'id' => 23,
                'name' => 'Papua Barat'
            ],
            [
                'id' => 24,
                'name' => 'Papua'
            ],
            [
                'id' => 25,
                'name' => 'Riau'
            ],
            [
                'id' => 26,
                'name' => 'Sulawesi Barat'
            ],
            [
                'id' => 27,
                'name' => 'Sulawesi Selatan'
            ],
            [
                'id' => 28,
                'name' => 'Sulawesi Tengah'
            ],
            [
                'id' => 29,
                'name' => 'Sulawesi Tenggara'
            ],
            [
                'id' => 30,
                'name' => 'Sulawesi Utara'
            ],
            [
                'id' => 31,
                'name' => 'Sumatra Barat'
            ],
            [
                'id' => 32,
                'name' => 'Sumatra Selatan'
            ],
            [
                'id' => 33,
                'name' => 'Sumatra Utara'
            ],
            [
                'id' => 34,
                'name' => 'Yogyakarta'
            ],
        ]);
    }
}
