<?php

namespace App\Http\Controllers\Dashboard\Member;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Models\RoleUser;
use App\Models\Member;
use App\Models\Province;
use App\Models\City;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function index()
    {
        return view('dashboard.member.all_member');
    }
    
    public function create()
    {
        $province = Province::get();
        $city = DB::table('provinces')->leftJoin('cities','provinces.id','=','cities.province_id')->get();

        return view('dashboard.member.add_member',compact('province','city'));
    }
    
    public function store(Request $request)
    {
        $user2 = User::where('id',Auth::user()->id)->first();
        $member = $user2->member()->first();

        $province = Province::where('id',$member->province)->first();
        $city = City::where('id',$member->city)->first();

        if (Auth::user()->hasRole('master')) 
        {
            $p = $request->province;
            $c = $request->city;
            
        }
        else
        {
            $p = $province->id;
            $c = $city->id;
        }

        $user = new User;

        $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required|unique:users',
            'password' => 'required|min:8'
        ]);
        
        $user->uuid = Uuid::uuid4();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->email_verified_at = now();
        $user->password = bcrypt($request->password);
        $user->save();
        
        //Kode Jabatan 
        if ($request->position == 'admin') 
        {
            $jbt = 1;
        }
        elseif ($request->position == 'member') 
        {
            $jbt = 2;
        }

        $memberid = $jbt.date('ym').$user->id;

        Member::create(
            [
                'user_id'=>$user->id,
                'memberid' => $memberid,
                'address' => $request->address,
                'province' => $p,
                'city' => $c,
                'hp' => $request->hp,
                'birth_date' => date('Y-m-d',strtotime($request->birth_date)),
                'birth_place' => $request->birth_place,
                'gender' => $request->gender,
                'education' => $request->education,
                'status' => $request->status,
                'job' => $request->job,
                'skill' => $request->skill,
                'fb' => $request->fb,
                'position' => $request->position
            ]
        );

        $member = Member::where('user_id','=',$user->id)->first();

        if ($request->hasFile('photo')) 
        {
            $photo = $request->file('photo');
            $photoName = $user->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/member',$photoName);
            $member->photo = $photoName;
        }

        $member->update();

        if ($request->position == 'admin') 
        {
            RoleUser::create(['user_id' => $user->id,'role_id' => 2]);
            RoleUser::create(['user_id' => $user->id,'role_id' => 3]);
        }
        elseif ($request->position == 'member') 
        {
            RoleUser::create(['user_id' => $user->id,'role_id' => 3]);
        }

        return redirect()->route('home')->with('success','Berhasil Menambahkan'); 
    }

    
    public function show($id)
    {
        $user = User::where('uuid',$id)->first();
        $member = $user->member()->first();
        $province = Province::where('id',$member->province)->first();
        $city = City::where('id',$member->city)->first();

        return view('dashboard.member.detail_member',compact('user','member','province','city'));
    }
    
    public function edit($id)
    {
        $user = User::where('uuid',$id)->first();
        $member = $user->member()->first();
        $province = Province::get();
        $city = DB::table('provinces')->leftJoin('cities','provinces.id','=','cities.province_id')->get();
        $province2 = Province::where('id',$member->province)->first();
        $city2 = City::where('id',$member->city)->first();

        return view('dashboard.member.edit_member',compact('user','member','province','city','province2','city2'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required',
            'password' => 'required|min:8'
        ]);

        $user2 = User::where('id',Auth::user()->id)->first();
        $member = $user2->member()->first();

        $province = Province::where('id',$member->province)->first();
        $city = City::where('id',$member->city)->first();

        if (Auth::user()->hasRole('master')) 
        {
            $p = $request->province;
            $c = $request->city;
            
        }
        else
        {
            $p = $province->id;
            $c = $city->id;
        }
        
        $user = User::where('uuid',$id)->first();
        $member = $user->member()->first();

        //============ User =========
        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password != $user->password) 
        {
            $user->password = bcrypt($request->password);
        }
        $user->update();

        //=========== member =============
        $member->province = $p;
        $member->city = $c;
        $member->address = $request->address;
        $member->hp = $request->hp;
        $member->birth_date = date('Y-m-d', strtotime($request->birth_date));
        $member->birth_place = $request->birth_place;
        $member->gender = $request->gender;
        $member->education = $request->education;
        $member->status = $request->status;
        $member->job = $request->job;
        $member->skill = $request->skill;
        $member->fb = $request->fb;

        if ($request->hasFile('photo')) 
        {
            $photo = $request->file('photo');
            $photoName = $user->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/member',$photoName);
            $member->photo = $photoName;
        }
        $member->update();

        return redirect()->back()->with('success','Edit Success');
    }
    
    public function destroy($id)
    {
        $member = Member::find($id);
        $user = User::where('id',$member->user_id)->first();
        // dd($user);

        $user->delete();
    }

    public function memberApi(){
        $member = DB::table('users')->rightJoin('members','members.user_id','=','users.id');

        return $this->displayMember($member);
    }
}
