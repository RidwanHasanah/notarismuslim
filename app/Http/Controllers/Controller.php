<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Yajra\DataTables\DataTables;
use App\Models\Province;
use App\Models\City;



class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function displayMember($member)
    {
        return DataTables::of($member)
        ->editColumn('province', function($member){
            $province = Province::where('id',$member->province)->first();
            return $province->name;
        })

        ->addColumn('action', function ($member) {
            $url = route('member.show',$member->uuid);
            return "<a style='margin-top:0.5em;' href='".$url."' class='btn btn-info btn-sm' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em;' href='".route('member.edit',$member->uuid)."' class='btn btn-success btn-sm' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em;' onclick='deleteMember(".$member->id.")' class='btn btn-danger btn-sm text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->make(true);
    }
}
