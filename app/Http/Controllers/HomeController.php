<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\City;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Member;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $user = User::where('id',Auth::user()->id)->first();
        $member = $user->member()->first();

        $province = Province::where('id',$member->province)->first();
        $city = City::where('id',$member->city)->first();

        // if (Auth::user()->hasRole('master')) 
        // {
            $data = [
                'member' => count(Member::get()),
                'pria'   => count(Member::where('gender','pria')->get()),
                'wanita' => count(Member::where('gender','wanita')->get()),
    
            ];
        // }
        // else
        // {
        //     $data = [
        //         'member' => count(member::where([['province','=',$member->province],['city','=',$member->city]])->get()),
        //         'pria' => count(member::where([['province','=',$member->province],['city','=',$member->city],['gender','=','pria']])->get()),
        //         'wanita' => count(member::where([['province','=',$member->province],['city','=',$member->city],['gender','=','wanita']])->get()),
    
        //     ];
        // }
        

        return view('dashboard.home.home',compact('data','province','city'));
    }
}
