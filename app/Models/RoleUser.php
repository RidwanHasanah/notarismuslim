<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $guarded = ['id'];
    protected $table = 'role_users';
    public $timestamps = false;
}
