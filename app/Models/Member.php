<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $guarded = ['id'];
    protected $table = 'members';

    public function user(){
        return $this->belongsTo('App\User');
    }
}
