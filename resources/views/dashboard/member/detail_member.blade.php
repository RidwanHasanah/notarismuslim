@extends('dashboard.master')
@section('title')
    Detail Member
@endsection
@section('content')
<div class="main-content">
  @include('dashboard.components.menu_dropdown')
  <div class="header bg-gradient-primary  pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row">
          <div class="col-md-3">
            <div class="card bg-gradient-default shadow">
              <div class="card-body">
                  @if (isset($member->photo))
                  <img class="img-thumbnail d-block" src="{{asset('storage/member/'.$member->photo)}}" id='img-upload' alt="">
                  @else
                  <img class="img-thumbnail d-block" src="{{asset('img/user.png')}}" id='img-upload' alt="">
                  @endif
              </div>
            </div>
          </div>

          <div class="col-md-9">
            <div class="card bg-gradient-default shadow">
              <div class="card-body">
                <table class="table align-items-center table-flush">
                  <tr>
                    <td>Nama</td>
                    <td>{{$user->name}}</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>{{$user->email}}</td>
                  </tr>
                  <tr>
                    <td>Handphone</td>
                    <td>{{$member->hp}}</td>
                  </tr>
                  <tr>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>{{$member->birth_place}} , {{date('d - m - Y',strtotime($member->birth_date))}}</td>
                  </tr>
                  <tr>
                    <td>Pendidikan</td>
                    <td>{{$member->education}}</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--7 mb-5">
    <div class="row">
      <div class="col-md-6">
        <div class="card bg-gradient-default shadow">
          <div class="card-body">
            <table class="table align-items-center table-flush">
              
              <tr>
                <td>Status Perkawinan</td>
                <td>{{$member->status}}</td>
              </tr>
              <tr>
                <td>Pekerjaan</td>
                <td>{{$member->job}}</td>
              </tr>
              <tr>
                <td>Keahlian</td>
                <td>{{$member->skill}}</td>
              </tr>
              <tr>
                <td>Akun Facebook</td>
                <td><a class="btn btn-info btn-sm" href="{{$member->fb}}" target="_blank"> Facebook</a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="card bg-gradient-default shadow">
          <div class="card-body">
            <table class="table align-items-center table-flush">
              <tr>
                <td>Jenis Kelamin</td>
                <td>{{$member->gender}}</td>
              </tr>
              <tr>
                <td>Provinsi</td>
                <td>{{$province->name}}</td>
              </tr>
              <tr>
                <td>Kota</td>
                <td>{{$city->name}}</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>{{$member->address}}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/jquery-chain.js')}}"></script>
@endsection