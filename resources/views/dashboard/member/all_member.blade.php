@extends('dashboard.master')
@section('content')
<div class="main-content">
    @include('dashboard.components.menu_dropdown')
    <div class="header bg-gradient-primary  pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
        </div>
    </div>
    <div class="container-fluid mt--7 mb-5">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Semua Anggota</h3>
                    </div>
                    <div class="table-responsive p-3">
                        <table id="table" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">memberid</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Handphone</th>
                                    <th scope="col">Gender</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            {{-- <tbody>
                                <tr>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <a href="#" class="avatar rounded-circle mr-3">
                                                <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                                            </a>
                                            <div class="media-body">
                                                <span class="mb-0 text-sm">Argon Design System</span>
                                            </div>
                                        </div>
                                    </th>
                                    <td>
                                        $2,500 USD
                                    </td>
                                    <td>
                                        <span class="badge badge-dot mr-4">
                                            <i class="bg-warning"></i> pending
                                        </span>
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"
                                                x-placement="bottom-end"
                                                style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(32px, 32px, 0px);">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody> --}}
                        </table>
                    </div>
                    {{-- <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">
                                        <i class="fas fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <i class="fas fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
    function getLastUrl(){
          var url = window.location.pathname
          var split = url.split("/")
          var last = split.pop()
          return last
    }

    if(getLastUrl() == 'Komandan'){
      var ajaxApi = '{{route("api.member","Komandan")}}' 
    }else if(getLastUrl() == 'Sekretaris'){
      var ajaxApi = '{{route("api.member","Sekretaris")}}'
    }else if(getLastUrl() == 'Bendahara'){
      var ajaxApi = '{{route("api.member","Bendahara")}}'
    }else if(getLastUrl() == 'Customer%20Service'){
      var ajaxApi = '{{route("api.member","Customer Service")}}'
    }else if(getLastUrl() == 'Paskas%20Member'){
      var ajaxApi = '{{route("api.member","Paskas Member")}}'
    }

    var table = $('#table').DataTable({
        order: [[1,'desc']],
        processing: true,
        serverSide: true,
        ajax : '{{route("api.member")}}',
        columns : [
            {data: 'memberid', name: 'memberid'},
            {data: 'name', name: 'name'},
            {data: 'hp', name: 'hp'},
            {data: 'gender', name: 'gender'},
            {data: 'action', name: 'action',ordertable: false, searchable:false},
        ]

    })
    

    // Delete Data
    function deleteMember(id){
        // alert('bismillah');
        
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        Swal.fire({
          title: 'Kamu yakin ?',
          text: "Member yang di hapus tidak akan kembali!",
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Ya, Hapus ini!'
        }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "{{ url('member') }}" + '/' + id,
            type: "DELETE",
            data: {'_method' : 'DELETE', '_token' : csrf_token},
            success: function(data){
              table.ajax.reload();
              Swal.fire({
                title:'Berhasil',
                text: 'User Sudah di hapus',
                type: 'success',
                timer: '1500'
              })
            },
            error: function(){
              Swal.fire({
                title: 'Oops',
                text: 'Something went wrong',
                type: 'error',
                timer: '1500'
              })
            }
          })
        }
      })
    }

    
    </script>
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>

@endsection