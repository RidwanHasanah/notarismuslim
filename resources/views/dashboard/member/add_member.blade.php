@extends('dashboard.master')
@section('content')
<div class="main-content">
  @include('dashboard.components.menu_dropdown')
  <form class="mb-5" action="{{route('member.store')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}} {{ method_field('POST')}}
    <div class="header bg-gradient-primary  pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block pb-5">Tambah Anggota</a>
        <div class="header-body">
          <div class="row">

            <div class="col-md-3">
              <div class="card bg-gradient-default shadow">
                <div class="card-body">
                    <img class="img-thumbnail d-block" src="{{asset('img/user.png')}}" id='img-upload' alt="">
                    <br>
                    <div class="input-group upload">
                      <label class="btn btn-primary shadow" for="photo">Upload Foto</label>
                      <input class="form-control d-none" value="{{ old('photo') }}" accept="image/jpeg,image/jpg,image/png,"
                        type="file" name="photo" id="photo">
                      <span class="errorval errorRegis" id="error_photo"></span>
                    </div>
                </div>
              </div>
            </div>

            <div class="col-md-9">
                <div class="card bg-gradient-default shadow">
                    <div class="card-body">
              <div class="form-group">
                <input required type="text" class="form-control" name="name" value="{{ old('name') }}" id="name"
                  placeholder="Masukan Nama">
                @error('name')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>

              <div class="form-group">
                <input value="{{ old('email') }}" required type="email" class="form-control" name="email" id="email"
                  placeholder="Masukan Email">
                @error('email')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>

              <div class="form-group">
                <input required type="password" class="form-control" id="password" name="password"
                  placeholder="Password">
                @error('password')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
            </div>
          </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--7 mb-5">
      <div class="row">
        <div class="col-md-6">
          <div class="card bg-gradient-default shadow">
            <div class="card-body">
              <div class="form-group">
                <input type="number" class="form-control" name="hp" value="{{ old('hp') }}" id="hp"
                  placeholder="Handphone">
                @error('hp')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
              <div class="form-group">
                <input style="cursor:pointer;" readonly type="text" class="form-control" name="birth_date"
                  value="{{ old('birth_date') }}" id="datepicker" placeholder="Tanggal Lahir">
                @error('birth_date')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="birth_place" value="{{ old('birth_place') }}"
                  id="birth_place" placeholder="Tempat Lahir">
                @error('birth_place')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="education" value="{{ old('education') }}" id="education"
                  placeholder="Pendidikan">
                @error('education')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
              <div class="form-group">
                <label class="text-white" for="income">Status Perkawinan</label>
                <select name="status" id="status" class="form-control">
                  <option {{old('status')=='Belum Kawin'?'selected':''}} value="Belum Kawin">Belum Kawin</option>
                  <option {{old('status')=='Sudah Kawin'?'selected':''}} value="Sudah Kawin">Sudah Kawin</option>
                </select>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="job" value="{{ old('job') }}" id="job"
                  placeholder="Pekerjaan">
                @error('job')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="skill" value="{{ old('skill') }}" id="skill"
                  placeholder="Keahlian">
                @error('skill')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
              <div class="form-group">
                <input type="link" class="form-control" name="fb" value="{{ old('fb') }}" id="fb"
                  placeholder="Akun Facebook">
                @error('fb')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="card bg-gradient-default shadow">
            <div class="card-body">
              <div class="form-group">
                <label class="col-form-label text-white">Jenis Kelamin</label>
                <div class="custom-control custom-radio mb-3">
                    <input name="gender" class="custom-control-input" value="Pria" id="gender1" checked type="radio">
                    <label class="custom-control-label text-white" for="gender1">Pria</label>
                  </div>
                  <div class="custom-control custom-radio mb-3">
                    <input name="gender" class="custom-control-input" value="Wanita" id="gender2" type="radio">
                    <label class="custom-control-label text-white" for="gender2">Wanita</label>
                  </div>
              </div>

              <div class="form-group">
                <label for="" class="text-white">Posisi</label>
                <select class="form-control" name="position" id="position">
                  @if (Auth::user()->hasRole('master'))
                  <option {{ old('position')=='admin'?'selected':'' }} value="admin">Admin</option>
                  @endif
                  <option {{ old('position')=='member'?'selected':'' }} value="member">member</option>
                </select>
              </div>

              <div class="form-group">
                <label for="" class="text-white">Provinsi</label>
                <select class="form-control" name="province" id="province">
                  {{-- <option value="">----------</option> --}}
                  @foreach ($province as $province)
                  <option {{ old('province')==$province->id?'selected':'' }} value="{{ $province->id }}">
                    {{ $province->name }}</option>
                  @endforeach
                </select>
                @error('province')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>

              <div class="form-group">
                <label id="city2" for="city" class="text-white">Kota</label>
                <select class="form-control" name="city" id="city">
                  {{-- <option value="">----------</option> --}}
                  @foreach ($city as $city)
                  <option class="{{ $city->province_id }}" value="{{ $city->id }}">{{$city->name}}</option>
                  @endforeach
                </select>
                @error('city')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>

              <div class="form-group">
                <label class="text-white" for="">Alamat</label>
                <textarea type="text" class="form-control" name="address" id="address"
                  placeholder="">{{ old('address') }}</textarea>
                @error('address')
                <span class="mt-1 alert-danger" role="alert">
                  <small>{{ $message }}</small>
                </span>
                @enderror
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <button type="submit" class="btn btn-primary mr-2 btn-block">Selesai</button>
  </form>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/jquery-chain.js')}}"></script>
@endsection