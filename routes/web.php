<?php

Route::get('/', function(){ return view('auth.login'); });

Route::get('/user-profile', function () {
    return view('dashboard/yourprofile');
});

Route::resource('reg', 'Auth\MyRegisterController');
Auth::routes();

Route::group(['middleware'=>['auth','verified']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    // ===== Member =====
    Route::resource('member', 'Dashboard\Member\MemberController');
    Route::resource('profile', 'Dashboard\Member\ProfileController');
    
    Route::get('api.member','Dashboard\Member\MemberController@memberApi')->name('api.member');

    Route::get('password/{uuid}/edit','Dashboard\Member\ProfileController@passwordEdit')->name('password.edit');
    Route::patch('password/{uuid}','Dashboard\Member\ProfileController@passwordUpdate')->name('password.update');


});
